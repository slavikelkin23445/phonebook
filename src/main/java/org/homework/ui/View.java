package org.homework.ui;

import org.homework.UserContactType;

import java.util.List;
import java.util.Scanner;

public class View {
    private Scanner scanner;

    public View(Scanner scanner) {
        this.scanner = scanner;
    }


    public UserContactType getContactType() {
        System.out.println("Add contact with 1 - phone number 2 - email");
        int num = scanner.nextInt();
        scanner.nextLine();
        return num == 1 ? UserContactType.PHONE : UserContactType.EMAIL;
    }

    public String getName() {
        System.out.println("Enter contact name:");
        return scanner.nextLine();
    }

    public String getSurname() {
        System.out.println("Enter contact surname:");
        return scanner.nextLine();
    }

    public String getPatronymic() {
        System.out.println("Enter contact patronymic:");
        return scanner.nextLine();
    }

    public String getPhoneNumber() {
        System.out.println("Enter contact phone number:");
        return scanner.nextLine();
    }

    public String getEmail() {
       System.out.println("Enter contact email:");
       return scanner.nextLine();
    }

    public String getSearcheableName(){
        System.out.println("Please, enter name you're looking for: ");
        return scanner.nextLine();
    }

    public String getSearcheableContact(){
        System.out.println("Please, enter beginning of numbers or emails: ");
        return scanner.nextLine();
    }

    public int getUserChoice(){
        System.out.println("Add one more contact 1 - Yes, 2 - No: ");
        int num = scanner.nextInt();
        scanner.nextLine();
        return num;
    }

    public void showList(List cl){
        for (Object c: cl) {
            System.out.println(c);
        }
    }
}
