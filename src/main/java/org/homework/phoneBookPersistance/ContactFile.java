package org.homework.phoneBookPersistance;

import org.homework.phoneBookEntity.Contact;

import java.util.List;


public interface ContactFile {
    List<Contact> readAll();
    void saveAll(List<Contact> toSave);
}
