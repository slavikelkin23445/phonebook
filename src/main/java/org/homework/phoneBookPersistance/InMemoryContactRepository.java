package org.homework.phoneBookPersistance;

import lombok.AllArgsConstructor;
import org.homework.UserContactType;
import org.homework.phoneBookEntity.Contact;
import org.homework.phoneBookUtil.ContactList;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
public class InMemoryContactRepository implements ContactRepository {

    ContactList contactList;

    @Override
    public void addContact(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        contactList.getList().add(contact);
    }

    @Override
    public void deleteContact(String value) {
        contactList.getList().removeAll(searchContactByBeginning(value));
    }

    @Override
    public List<Contact> searchContactByBeginning(String value) {
        return contactList.getList()
                .stream()
                .filter(i-> i.getValue().contains(value))
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> searchByName(String value) {
        return contactList.getList()
                .stream()
                .filter(i-> i.getName().contains(value))
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> onlyEmails(UserContactType type) {
        return contactList.getList()
                .stream()
                .filter(i -> i.getType() == type)
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> onlyPhones(UserContactType type) {
        return contactList.getList()
                .stream()
                .filter(i -> i.getType() == type)
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findAll() {
        return contactList.getList().stream()
                .sorted(Contact::compareTo)
                .collect(Collectors.toList());

    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return contactList.getList().stream()
                .filter(c->c.getValue().equals(value))
                .findFirst();
    }

}
