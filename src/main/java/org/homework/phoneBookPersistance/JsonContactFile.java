package org.homework.phoneBookPersistance;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.homework.phoneBookEntity.Contact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class JsonContactFile implements ContactFile {

    private ObjectMapper objectMapper;
    private String fileName;

    @Override
    public List<Contact> readAll() {
        try(InputStream fis = new FileInputStream(fileName)){
            return objectMapper.readValue(fis, new TypeReference<ArrayList<Contact>>() {});
        }catch(IOException e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void saveAll(List<Contact> contacts) {
        try(OutputStream os = new FileOutputStream(fileName)){
            byte[] jsonBytes = objectMapper.writeValueAsBytes(contacts);
            os.write(jsonBytes);
            os.flush();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
