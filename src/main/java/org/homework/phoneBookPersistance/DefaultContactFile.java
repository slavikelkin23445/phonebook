package org.homework.phoneBookPersistance;

import lombok.AllArgsConstructor;
import org.homework.UserContactType;
import org.homework.phoneBookEntity.Contact;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@AllArgsConstructor
public class DefaultContactFile implements ContactFile{

    private String fileName;

    @Override
    public List<Contact> readAll() {
        List<Contact> temp = new ArrayList<>();
        try(InputStream is = new FileInputStream(fileName)){
            Scanner scanner = new Scanner(is);
            while(scanner.hasNextLine()) {
                Contact contact = new Contact();
                String line = scanner.nextLine();
                if(line.trim().isEmpty()) continue;
                String[] parts = line.split("\\,");
                String[] data = new String[parts.length];
                for (int i = 0; i < data.length; i++) {
                    data[i] = parts[i].substring(parts[i].lastIndexOf(":") + 1);
                    data[i] = data[i].replace("\"", "");
                }
                contact.setName(data[0]);
                contact.setSurname(data[1]);
                contact.setPatronymic(data[2]);
                contact.setType(UserContactType.PHONE.name().compareTo(data[3]) == 0 ? UserContactType.PHONE
                        :UserContactType.EMAIL);
                contact.setValue(data[4]);
                contact.setId(data[5]);
                temp.add(contact);
            }
            return temp;
        }catch (IOException e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void saveAll(List<Contact> contacts) {
        try(OutputStream os = new FileOutputStream(fileName)){
            for (Contact contact : contacts) {
                os.write(contact.toString().getBytes(StandardCharsets.UTF_8));
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
