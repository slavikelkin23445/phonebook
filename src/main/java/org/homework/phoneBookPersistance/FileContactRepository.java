package org.homework.phoneBookPersistance;

import lombok.AllArgsConstructor;
import org.homework.UserContactType;
import org.homework.phoneBookEntity.Contact;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
public class FileContactRepository implements ContactRepository{

    private final ContactFile contactFile;

    @Override
    public void addContact(Contact contact) {
        List<Contact> contacts = contactFile.readAll();
        contact.setId(UUID.randomUUID().toString());
        contacts.add(contact);
        contactFile.saveAll(contacts);
    }

    @Override
    public void deleteContact(String value) {
        List<Contact> toSave = contactFile.readAll().stream()
                .filter(c->!c.getValue().equals(value))
                .collect(Collectors.toList());
        contactFile.saveAll(toSave);
    }

    @Override
    public List<Contact> searchContactByBeginning(String value) {
        return contactFile.readAll().stream()
                .filter(i-> i.getValue().contains(value))
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> searchByName(String value) {
        return contactFile.readAll()
                .stream()
                .filter(i-> i.getName().contains(value))
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> onlyEmails(UserContactType type) {
        return contactFile.readAll()
                .stream()
                .filter(i -> i.getType() == type)
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> onlyPhones(UserContactType type) {
        return contactFile.readAll()
                .stream()
                .filter(i -> i.getType() == type)
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findAll() {
        return contactFile.readAll();
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return contactFile.readAll().stream()
                .filter(c->c.getValue().equals(value))
                .findFirst();
    }
}
