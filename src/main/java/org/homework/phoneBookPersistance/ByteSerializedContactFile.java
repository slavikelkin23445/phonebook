package org.homework.phoneBookPersistance;

import lombok.AllArgsConstructor;
import org.homework.phoneBookEntity.Contact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class ByteSerializedContactFile implements ContactFile{
    private String fileName;

    @Override
    public List<Contact> readAll() {
        List<Contact> temp = null;
        try(ObjectInputStream is = new ObjectInputStream(new FileInputStream(fileName))){
            int size = is.readInt();
            temp = new ArrayList<>(size);
            temp.addAll((ArrayList<Contact>) is.readObject());
            return temp;
        }catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void saveAll(List<Contact> contacts) {
        try(ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName))){
            os.writeInt(contacts.size());
            os.writeObject(contacts);
            os.flush();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
