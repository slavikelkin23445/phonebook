package org.homework.phoneBookPersistance;

import org.homework.UserContactType;
import org.homework.phoneBookEntity.Contact;

import java.util.List;
import java.util.Optional;

public interface ContactRepository {
    void addContact(Contact contact);
    void deleteContact(String value);
    List<Contact> searchContactByBeginning(String value);
    List<Contact> searchByName(String value);
    List<Contact> onlyEmails(UserContactType type);
    List<Contact> onlyPhones(UserContactType type);
    List<Contact> findAll();
    Optional<Contact> findByValue(String value);
}
