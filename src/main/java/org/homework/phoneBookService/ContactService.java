package org.homework.phoneBookService;

import lombok.AllArgsConstructor;
import org.homework.UserContactType;
import org.homework.exception.DuplicateContactException;
import org.homework.phoneBookEntity.Contact;
import org.homework.phoneBookPersistance.ContactRepository;

import java.util.List;

@AllArgsConstructor
public class ContactService {

    private final ContactRepository contactRepository;

    public void addContact(Contact contact){
        if(contactRepository.findByValue(contact.getValue()).isPresent()){
            throw new DuplicateContactException();
        }
        contactRepository.addContact(contact);
    }

    public void deleteContact(String value){
        contactRepository.deleteContact(value);
    }

    public List<Contact> searchContactByBeginning(String value){
        return contactRepository.searchContactByBeginning(value);
    }

    public List<Contact> searchByName(String value){
        return contactRepository.searchByName(value);
    }
    public List<Contact> onlyEmails(){
        return contactRepository.onlyPhones(UserContactType.EMAIL);
    }

    public List<Contact> onlyPhones(){
        return contactRepository.onlyPhones(UserContactType.PHONE);
    }

    public List<Contact> findAll(){
            return contactRepository.findAll();
    }


}
