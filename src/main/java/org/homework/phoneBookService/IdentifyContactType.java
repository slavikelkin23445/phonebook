package org.homework.phoneBookService;

import org.homework.UserContactType;

public class IdentifyContactType {
    public UserContactType identifyContactType(int type) {
        UserContactType uct;
        for (UserContactType el : UserContactType.values()) {
            if (el.getId() == type) {
                uct = UserContactType.valueOf(UserContactType.class, el.toString());
                return uct;
            }
        }
        return null;
    }
}
