package org.homework.exception;

import java.security.InvalidParameterException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailCorrectionCheck {

    public void validateEmail(String email){
        StringBuilder temp = new StringBuilder(email);
        Pattern pattern = Pattern.compile("(\\w{1,64} ?(:?\\@\\w{1,253}(\\.(:?com|org|net))))");
        Matcher matcher = pattern.matcher(email);
        if(!matcher.matches()){
            throw new InvalidParameterException("Email must be [XXXX..]@[domain].[com|net|org] and include " +
                    "only [a-z] or [0-9] characters");
            }
        }
    }
