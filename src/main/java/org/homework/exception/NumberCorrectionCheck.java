package org.homework.exception;

import java.security.InvalidParameterException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberCorrectionCheck {
    public void validatePhone(String phone){
        if(phone == null) throw new InvalidParameterException("Phone number must be not null");

        Pattern pattern = Pattern.compile("(\\+380|380|80|0)\\d{9}");
        Matcher matcher = pattern.matcher(phone);
        if(!matcher.matches()){
            throw new InvalidParameterException("Phone number format is 0XXYYYYYYY");
        }
    }
}
