package org.homework.exception;

import java.security.InvalidParameterException;

public class NameAndSurnameNotNullCheck {

    public void notNullNameAndSurname(String name, String surname){
        if(name == "" && surname == "")
            throw new InvalidParameterException("Contact must contains at least name or surname");
    }
}
