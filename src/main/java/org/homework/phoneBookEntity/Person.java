package org.homework.phoneBookEntity;

public class Person {
    private String name;
    private String surname;
    private String patronymic;

    public Person(String name, String surname, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public String getFullName(){
        return new StringBuilder().append(surname + " " + name + " " + patronymic).toString();
    }
}
