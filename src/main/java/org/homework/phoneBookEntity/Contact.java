package org.homework.phoneBookEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.homework.UserContactType;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@Accessors(chain = true)
public class Contact implements Comparable<Contact>, Serializable {
    private String name;
    private String surname;
    private String patronymic;
    private UserContactType type;
    private String value;
    private String id;

    @Override
    public String toString() {
        return "name:\"" + name
                + "\"," + "surname:\"" + surname
                + "\"," + "patronymic:\"" + patronymic
                + "\"," + "type:\"" + type
                + "\"," + "value:\"" + value
                + "\"," + "ID:\"" + id
                + "\"" + "\n";
    }

    @Override
    public int compareTo(Contact o) {
        if(o == null) return 1;
        if(Objects.equals(name, o.name)
                && Objects.equals(surname, o.surname)
                && Objects.equals(patronymic, o.patronymic))
            return 0;

        int tmp = surname.compareTo(o.surname);
        if(tmp == 0) name.compareTo(o.name);
        if(tmp == 0) patronymic.compareTo(o.patronymic);
        return tmp;
    }
}
