package org.homework;

import lombok.Getter;
import lombok.Setter;

public enum UserContactType {
    PHONE(1), EMAIL(2);

    @Getter
    private int id;

    UserContactType(int id) {
        this.id = id;
    }
}
