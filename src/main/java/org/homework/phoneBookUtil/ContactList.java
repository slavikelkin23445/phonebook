package org.homework.phoneBookUtil;

import lombok.Getter;
import lombok.Setter;
import org.homework.phoneBookEntity.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactList{
    @Getter
    @Setter
    private List<Contact> list;
    {
        list = new ArrayList<>();
    }
}
