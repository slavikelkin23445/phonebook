package org.homework.menu;

public interface MenuItem {
    String getName();
    void execute();
    default boolean ifFinal(){
        return false;
    }
}
