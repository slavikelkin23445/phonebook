package org.homework.menuItem;

import lombok.AllArgsConstructor;
import org.homework.UserContactType;
import org.homework.menu.MenuItem;
import org.homework.phoneBookEntity.Contact;
import org.homework.phoneBookService.ContactService;
import org.homework.ui.View;

import java.util.Scanner;


@AllArgsConstructor
public class AddContactMenuItem implements MenuItem {

    private final ContactService contactService;
    private Scanner scanner;
    private View view;
    @Override
    public String getName() {
        return "Add Contact";
    }

    @Override
    public void execute() {
        Contact contact = new Contact();
        contact.setName(view.getName());
        contact.setSurname(view.getSurname());
        contact.setPatronymic(view.getPatronymic());
        contact.setType(view.getContactType());
        contact.setValue(contact.getType() == UserContactType.PHONE ? view.getPhoneNumber() : view.getEmail());
        contactService.addContact(contact);
    }
}
