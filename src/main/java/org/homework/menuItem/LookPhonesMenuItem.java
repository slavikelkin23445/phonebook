package org.homework.menuItem;

import lombok.RequiredArgsConstructor;
import org.homework.menu.MenuItem;
import org.homework.phoneBookService.ContactService;
import org.homework.ui.View;

@RequiredArgsConstructor
public class LookPhonesMenuItem implements MenuItem {
    private final ContactService contactService;
    private final View view;
    @Override
    public String getName() {
        return "Look phones";
    }

    @Override
    public void execute() {
        view.showList(contactService.onlyPhones());
    }
}
