package org.homework.menuItem;

import lombok.RequiredArgsConstructor;
import org.homework.menu.MenuItem;
import org.homework.phoneBookService.ContactService;
import org.homework.ui.View;

import java.util.Scanner;

@RequiredArgsConstructor
public class DeleteContactByValueMenuItem implements MenuItem {
    private final ContactService contactService;
    private final Scanner scanner;

    @Override
    public String getName() {
        return "Delete contact by value";
    }

    @Override
    public void execute() {
        contactService.deleteContact(new View(scanner).getSearcheableContact());
    }
}
