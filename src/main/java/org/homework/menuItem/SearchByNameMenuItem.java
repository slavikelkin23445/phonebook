package org.homework.menuItem;

import lombok.RequiredArgsConstructor;
import org.homework.menu.MenuItem;
import org.homework.phoneBookService.ContactService;
import org.homework.ui.View;

import java.util.Scanner;

@RequiredArgsConstructor
public class SearchByNameMenuItem implements MenuItem {
    private final ContactService contactService;
    private final Scanner scanner;
    private final View view;

    @Override
    public String getName() {
        return "Search by name";
    }

    @Override
    public void execute() {
        view.showList(contactService.searchByName(new View(scanner).getSearcheableName()));
    }
}
