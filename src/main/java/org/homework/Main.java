package org.homework;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.homework.menu.Menu;
import org.homework.menu.MenuItem;
import org.homework.menuItem.*;
import org.homework.phoneBookPersistance.*;
import org.homework.phoneBookService.ContactService;
import org.homework.phoneBookUtil.ContactList;
import org.homework.ui.View;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ObjectMapper objectMapper = new ObjectMapper();

        ContactFile contactFile = new DefaultContactFile("Default.txt");

        ContactList contactList = new ContactList();
        ContactRepository contactRepository = new FileContactRepository(contactFile);
        ContactService contactService = new ContactService(contactRepository);

        Scanner scanner = new Scanner(System.in);
        View view = new View(scanner);

        Menu menu = new Menu(scanner, new MenuItem[]{
            new AddContactMenuItem(contactService, scanner, view),
            new LookContactsWithSortedSurnameMenuItem(contactService, view),
            new LookPhonesMenuItem(contactService, view),
            new LookEmailMenuItem(contactService, view),
            new SearchByNameMenuItem(contactService,scanner, view),
            new SearchByContactBeginningMenuItem(contactService,scanner, view),
            new DeleteContactByValueMenuItem(contactService, scanner),
            new ExitMenuItem()
        });
        menu.run();
    }
}
